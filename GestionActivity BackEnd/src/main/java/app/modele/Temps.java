package app.modele;

public class Temps {

	private int id;
	private String periode;
	private boolean validation;
	
	public Temps() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Temps(int id , String periode, boolean validation) {
		super();
		this.id=id;
		this.periode = periode;
		this.validation = validation;
	}
	public Temps(String periode, boolean validation) {
		super();
	//	this.id=id;
		this.periode = periode;
		this.validation = validation;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the periode
	 */
	public String getPeriode() {
		return periode;
	}
	/**
	 * @param periode the periode to set
	 */
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	/**
	 * @return the validation
	 */
	public boolean isValidation() {
		return validation;
	}
	/**
	 * @param validation the validation to set
	 */
	public void setValidation(boolean validation) {
		this.validation = validation;
	}
}

package app.modele;

public class Frais {
	private int id;
	private String periode;
	private boolean payer;
	private boolean validation;
	
	public Frais() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Frais(int id, String periode, boolean payer, boolean validation) {
		super();
		this.id = id;
		this.periode = periode;
		this.payer = payer;
		this.validation = validation;
	}
	
	public Frais(String periode, boolean payer, boolean validation) {
		super();
		this.periode = periode;
		this.payer = payer;
		this.validation = validation;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the periode
	 */
	public String getPeriode() {
		return periode;
	}
	/**
	 * @param periode the periode to set
	 */
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	/**
	 * @return the payer
	 */
	public boolean isPayer() {
		return payer;
	}
	/**
	 * @param payer the payer to set
	 */
	public void setPayer(boolean payer) {
		this.payer = payer;
	}
	/**
	 * @return the validation
	 */
	public boolean isValidation() {
		return validation;
	}
	/**
	 * @param validation the validation to set
	 */
	public void setValidation(boolean validation) {
		this.validation = validation;
	}
	
	
}

package app.controle;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.modele.Temps;
import app.service.MesTempsService;

@CrossOrigin
@RestController
public class MesTempsController {
	
	@Autowired
	private MesTempsService mesTemps;
	
	@RequestMapping("/temps")
	public ArrayList<Temps> getTempsControl() {
		return mesTemps.getTemps();
	}
	
}

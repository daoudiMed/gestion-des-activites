package app.controle;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.modele.Frais;
import app.modele.Temps;
import app.service.MesFraisService;
import app.service.MesTempsService;

@CrossOrigin
@RestController
public class MesFraisController {
	
	@Autowired
	private MesFraisService mesFrais;
	
	@RequestMapping("/frais")
	public ArrayList<Frais> getFraisControl() {
		return mesFrais.getFrais();
	}
}

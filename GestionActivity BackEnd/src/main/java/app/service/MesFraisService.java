package app.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import app.modele.Frais;

@Service
public class MesFraisService {
	private ArrayList<Frais> mesFrais = new ArrayList<Frais>(){{
		add(new Frais(1,"Creer le 01/05/2018",true,false));
		add(new Frais(2,"Creer le 01/07/2018",false, true));
		add(new Frais(3,"Creer le 01/01/2019",true, true));
	}};
	
	public ArrayList<Frais> getFrais(){
		return mesFrais;
	}

	public MesFraisService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}

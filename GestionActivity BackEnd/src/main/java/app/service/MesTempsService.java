package app.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import app.modele.Temps;


@Service
public class MesTempsService {
	
	private ArrayList<Temps> mesTemps = new ArrayList<Temps>(){{
		add(new Temps(1,"Creer le 01/05/2018",true));
		add(new Temps(2,"Creer le 01/07/2018",false));
		add(new Temps(3,"Creer le 01/01/2019",false));
	}};
	
	public ArrayList<Temps> getTemps(){
		return mesTemps;
	}

	public MesTempsService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}

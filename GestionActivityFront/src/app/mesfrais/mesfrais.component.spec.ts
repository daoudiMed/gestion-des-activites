import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule, MatSortModule, MatTableModule } from '@angular/material';

import { MesfraisComponent } from './mesfrais.component';

describe('MesfraisComponent', () => {
  let component: MesfraisComponent;
  let fixture: ComponentFixture<MesfraisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesfraisComponent ],
      imports: [
        NoopAnimationsModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesfraisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MesfraisDataSource } from './mesfrais-datasource';
import { MesfraisService } from './mesfrais.service'

@Component({
  selector: 'app-mesfrais',
  templateUrl: './mesfrais.component.html',
  styleUrls: ['./mesfrais.component.css']
})

export class MesfraisComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MesfraisDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id','periode','payer','validation'];

  constructor(private mesfraisService: MesfraisService) {
    //console.log("Constructors");
    //this.dataSource = new MestempsDataSource(this.paginator, this.sort, this.mesTemspsService);
  }

  ngOnInit() {
    this.dataSource = new MesfraisDataSource(this.paginator, this.sort, this.mesfraisService);
  }
}

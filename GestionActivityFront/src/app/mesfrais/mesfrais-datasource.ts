import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import {MesfraisService} from './mesfrais.service'

// TODO: Replace this with your own data model type
export interface MesfraisItem {
  id : number;
  periode: string;
  payer: boolean;
  validation: boolean;
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: MesfraisItem[] = [
  {id: 1, periode: 'creer le 01/01/2019', payer:false, validation:false},
  {id: 2, periode: 'creer le 01/02/2019', payer:true, validation:false}
];

/**
 * Data source for the Mesfrais view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class MesfraisDataSource extends DataSource<MesfraisItem> {
  data: MesfraisItem[];
  erreur:boolean=false;

  constructor(private paginator: MatPaginator, private sort: MatSort, private mesfraisService: MesfraisService) {
    super();
    
    this.getData(this.mesfraisService);
  }

  getData(mesfraisService: MesfraisService){
    this.mesfraisService.get_data() 
    .then((temps:MesfraisItem[]) => {
      this.data = temps;
      //this.paginator.length = this.data.length;
    })
    .catch(
      (response)=>{
        this.erreur=false;
        console.log("Erroooor!!");
        console.log(response);
      }
    );
  }
  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<MesfraisItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginator's length
    this.paginator.length = this.data.length;

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: MesfraisItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: MesfraisItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'periode': return compare(a.periode, b.periode, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

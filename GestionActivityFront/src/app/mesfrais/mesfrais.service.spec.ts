import { TestBed } from '@angular/core/testing';

import { MesfraisService } from './mesfrais.service';

describe('MesfraisService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MesfraisService = TestBed.get(MesfraisService);
    expect(service).toBeTruthy();
  });
});

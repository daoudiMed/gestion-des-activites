import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MesfraisService {

  constructor(private _http:HttpClient) { }
  
  async get_data(){
    return await this._http.get('http://localhost:8080/frais').toPromise();
  }
  
}

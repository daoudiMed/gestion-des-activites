import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient,HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MynavComponent } from './mynav/mynav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatTableModule, MatPaginatorModule, MatSortModule, MatGridListModule, MatCardModule, MatMenuModule } from '@angular/material';
import { MesfraisComponent } from './mesfrais/mesfrais.component';
import { TableaubordComponent } from './tableaubord/tableaubord.component';
import {Routes, RouterModule} from '@angular/router';
import { MestempsComponent } from './mestemps/mestemps.component';
import { LoginComponent } from './login/login.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {FormsModule} from '@angular/forms';


const appRoutes:Routes= [
  {path:'',component: TableaubordComponent},
  {path:'frais',component: MesfraisComponent},
  {path:'temps',component: MestempsComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    MynavComponent,
    MesfraisComponent,
    TableaubordComponent,
    MestempsComponent,
    LoginComponent,
    ToolbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule,
  ],
  providers:[HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }

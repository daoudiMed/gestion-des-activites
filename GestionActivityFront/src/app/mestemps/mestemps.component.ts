import { Component, OnInit, ViewChild , OnChanges} from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MestempsDataSource } from './mestemps-datasource';
import { HttpClient } from '@angular/common/http';
import { MesTempsService } from './mes-temps.service';

@Component({
  selector: 'app-mestemps',
  templateUrl: './mestemps.component.html',
  styleUrls: ['./mestemps.component.css'],

  // providers:[HttpClient]
})
export class MestempsComponent implements OnInit ,  OnChanges {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MestempsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'periode', 'validation'];
  
  constructor(private mesTemspsService: MesTempsService) {
    //console.log("Constructors");
    //this.dataSource = new MestempsDataSource(this.paginator, this.sort, this.mesTemspsService);
  }

  ngOnInit() {
    this.dataSource = new MestempsDataSource(this.paginator, this.sort, this.mesTemspsService);
    //this.dataSource.paginator = this.paginator;
  }
  ngOnChanges() {
    // changes.prop contains the old and the new value...
    //this.dataSource = new MestempsDataSource(this.paginator, this.sort, this.mesTemspsService);
  }
}

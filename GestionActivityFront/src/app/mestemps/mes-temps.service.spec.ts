import { TestBed } from '@angular/core/testing';

import { MesTempsService } from './mes-temps.service';

describe('MesTempsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MesTempsService = TestBed.get(MesTempsService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MesTempsService {

  constructor(private _http:HttpClient) { }
  
  async get_data(){
    console.log("Service mes Temps!!");
    return await this._http.get('http://localhost:8080/temps').toPromise();
  }
}

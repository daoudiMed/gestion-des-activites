import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { MesTempsService } from './mes-temps.service';

import { OnInit ,  OnChanges } from '@angular/core';
import { getViewData } from '@angular/core/src/render3/state';

// TODO: Replace this with your own data model type
export interface MestempsItem {
  id: number;
  periode: string;
  validation: boolean;
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: MestempsItem[] = [
  {id: 1, periode: 'creer',validation:true},
  {id: 2, periode: 'creer',validation:false},
];

/**
 * Data source for the Mestemps view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */

export class MestempsDataSource extends DataSource<MestempsItem> implements OnChanges {
  data: MestempsItem[];
  erreur:boolean=false;
  mesTemspsServiceIn: MesTempsService

  constructor(private paginator: MatPaginator, private sort: MatSort, private mesTemspsService: MesTempsService) {
    super();
    this.getData(mesTemspsService);
    this.mesTemspsServiceIn = mesTemspsService;
  }

  getData(mesTemspsService: MesTempsService){
    this.mesTemspsService.get_data() 
    .then((temps:MestempsItem[]) => {
      this.data = temps;
      console.log(this.data);
      //this.paginator.length = this.data.length;
    });
  }

  ngOnChanges(){
    this.getData(this.mesTemspsServiceIn);
  } 
  
  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<MestempsItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.

    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginator's length
    this.paginator.length = this.data.length;

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: MestempsItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: MestempsItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.periode, b.periode, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
